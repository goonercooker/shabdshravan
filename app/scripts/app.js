(function () {

'use strict';

angular.module('ssApp', ['ui.router','ngResource'])
.config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
        
            // route for the home page
            .state('app', {
                url:'/',
                views: {
                    'header': {
                        templateUrl : 'views/header.html',
                        controller : 'HeaderController'
                    },
                    'content@': {
                        templateUrl : 'views/home.html',
                        controller  : 'IndexController'
                    },
                    'footer': {
                        templateUrl : 'views/footer.html',
                    }
                },
                resolve: {
                    'setUrl': function ($rootScope) {
                        $rootScope.imgUrl = 'images/home.jpg';
                        $rootScope.home = true;
                        $rootScope.coverText = 'Explore the universe of sounds and express your heart out to your loved ones';
                    },
                    'setActive' :  function ($rootScope) {
                        $rootScope.hActive = true;
                        $rootScope.mActive = false;
                        $rootScope.aActive = false;
                        $rootScope.slActive = false;
                        $rootScope.haActive = false;
                        $rootScope.ciActive = false;
                        $rootScope.cActive = false;
                    }
                }
            })

            //route for meet shabd shravan page
            .state('app.meetss', {
                url:'meetss',
                views: {
                    'header': {
                        templateUrl : 'views/header.html',
                        controller : 'HeaderController'
                    },
                    'content@': {
                        templateUrl : 'views/meetus.html',
                    },
                    'footer': {
                        templateUrl : 'views/footer.html',
                    }
                },
                resolve: {
                    'setUrl': function ($rootScope) {
                        $rootScope.imgUrl = 'images/meetss.jpg';
                        $rootScope.home = false;
                        $rootScope.coverText = '';
                    },
                    'setActive' :  function ($rootScope) {
                        $rootScope.hActive = false;
                        $rootScope.mActive = true;
                        $rootScope.aActive = false;
                        $rootScope.slActive = false;
                        $rootScope.haActive = false;
                        $rootScope.ciActive = false;
                        $rootScope.cActive = false;
                    }
                }

            })
        
            // route for the cochear implants page
            .state('app.cochlearimplants', {
                url:'cochlear-implants',
                views: {
                    'header': {
                        templateUrl : 'views/header.html',
                        controller : 'HeaderController'
                    },
                    'content@': {
                        templateUrl : 'views/ci.html',
                        controller  : 'AboutController'                  
                    },
                    'footer': {
                        templateUrl : 'views/footer.html',
                    }
                },
                resolve: {
                    'setUrl': function ($rootScope) {
                        $rootScope.imgUrl = 'images/cochlearbg.jpg';
                        $rootScope.home = false;
                        $rootScope.coverText = '';
                    },
                    'setActive' :  function ($rootScope) {
                        $rootScope.hActive = false;
                        $rootScope.mActive = false;
                        $rootScope.aActive = false;
                        $rootScope.slActive = false;
                        $rootScope.haActive = false;
                        $rootScope.ciActive = true;
                        $rootScope.cActive = false;
                    }
                }
            })
        
            // route for the contactus page
            .state('app.contactus', {
                url:'contactus',
                views: {
                    'header': {
                        templateUrl : 'views/header.html',
                        controller : 'HeaderController'
                    },
                    'content@': {
                        templateUrl : 'views/contactus.html',
                        controller  : 'ContactController'                  
                    },
                    'footer': {
                        templateUrl : 'views/footer.html',
                    }
                },
                resolve: {
                    'setUrl': function ($rootScope) {
                        $rootScope.imgUrl = 'images/contact.jpg';
                        $rootScope.home = false;
                        $rootScope.coverText = '';
                    },
                    'setActive' :  function ($rootScope) {
                        $rootScope.hActive = false;
                        $rootScope.mActive = false;
                        $rootScope.aActive = false;
                        $rootScope.slActive = false;
                        $rootScope.haActive = false;
                        $rootScope.ciActive = false;
                        $rootScope.cActive = true;
                    }
                }
            })

            // route for the hearing aids page
            .state('app.hearingaids', {
                url: 'hearing-aids',
                views: {
                    'header': {
                        templateUrl : 'views/header.html',
                        controller : 'HeaderController'
                    },
                    'content@': {
                        templateUrl : 'views/ha.html',
                        controller  : 'MenuController'
                    },
                    'footer': {
                        templateUrl : 'views/footer.html',
                    }
                },
                resolve: {
                    'setUrl': function ($rootScope) {
                        $rootScope.imgUrl = 'images/hearing-aids.jpg';
                        $rootScope.home = false;
                        $rootScope.coverText = '';
                    },
                    'setActive' :  function ($rootScope) {
                        $rootScope.hActive = false;
                        $rootScope.mActive = false;
                        $rootScope.aActive = false;
                        $rootScope.slActive = false;
                        $rootScope.haActive = true;
                        $rootScope.ciActive = false;
                        $rootScope.cActive = false;
                    }
                }
            })
            // route for the dropdown pages
            .state('app.nbhs', {
                url: 'newbornhearingservices',
                views: {
                    'header': {
                        templateUrl : 'views/header.html',
                        controller : 'HeaderController'
                    },
                    'content@': {
                        templateUrl : 'views/nbhs.html',
                    },
                    'footer': {
                        templateUrl : 'views/footer.html',
                    }
                },
                resolve: {
                    'setUrl': function ($rootScope) {
                        $rootScope.imgUrl = 'images/nbhs.jpg';
                        $rootScope.home = false;
                        $rootScope.coverText = 'AUDIOLOGY';
                    },
                    'setActive' :  function ($rootScope) {
                        $rootScope.hActive = false;
                        $rootScope.mActive = false;
                        $rootScope.aActive = true;
                        $rootScope.slActive = false;
                        $rootScope.haActive = false;
                        $rootScope.ciActive = false;
                        $rootScope.cActive = false;
                    }
                }
            })
            .state('app.pha', {
                url: 'paediatrichearingassessment',
                views: {
                    'header': {
                        templateUrl : 'views/header.html',
                        controller : 'HeaderController'
                    },
                    'content@': {
                        templateUrl : 'views/pha.html',
                    },
                    'footer': {
                        templateUrl : 'views/footer.html',
                    }
                },
                resolve: {
                    'setUrl': function ($rootScope) {
                        $rootScope.imgUrl = 'images/pha.jpeg';
                        $rootScope.home = false;
                        $rootScope.coverText = 'AUDIOLOGY';
                    },
                    'setActive' :  function ($rootScope) {
                        $rootScope.hActive = false;
                        $rootScope.mActive = false;
                        $rootScope.aActive = true;
                        $rootScope.slActive = false;
                        $rootScope.haActive = false;
                        $rootScope.ciActive = false;
                        $rootScope.cActive = false;
                    }
                }
            })
            .state('app.aha', {
                url: 'adulthearingassessment',
                views: {
                    'header': {
                        templateUrl : 'views/header.html',
                        controller : 'HeaderController'
                    },
                    'content@': {
                        templateUrl : 'views/aha.html',
                    },
                    'footer': {
                        templateUrl : 'views/footer.html',
                    }
                },
                resolve: {
                    'setUrl': function ($rootScope) {
                        $rootScope.imgUrl = 'images/aha.jpg';
                        $rootScope.home = false;
                        $rootScope.coverText = 'AUDIOLOGY';
                    },
                    'setActive' :  function ($rootScope) {
                        $rootScope.hActive = false;
                        $rootScope.mActive = false;
                        $rootScope.aActive = true;
                        $rootScope.slActive = false;
                        $rootScope.haActive = false;
                        $rootScope.ciActive = false;
                        $rootScope.cActive = false;
                    }
                }
            })
            .state('app.apd', {
                url: 'auditoryprocessingdisorders',
                views: {
                    'header': {
                        templateUrl : 'views/header.html',
                        controller : 'HeaderController'
                    },
                    'content@': {
                        templateUrl : 'views/apd.html',
                    },
                    'footer': {
                        templateUrl : 'views/footer.html',
                    }
                },
                resolve: {
                    'setUrl': function ($rootScope) {
                        $rootScope.imgUrl = 'images/apd.jpg';
                        $rootScope.home = false;
                        $rootScope.coverText = 'AUDIOLOGY';
                    },
                    'setActive' :  function ($rootScope) {
                        $rootScope.hActive = false;
                        $rootScope.mActive = false;
                        $rootScope.aActive = true;
                        $rootScope.slActive = false;
                        $rootScope.haActive = false;
                        $rootScope.ciActive = false;
                        $rootScope.cActive = false;
                    }
                }
            })
            .state('app.slca', {
                url: 'speechlanguagecomprehensiveassessment',
                views: {
                    'header': {
                        templateUrl : 'views/header.html',
                        controller : 'HeaderController'
                    },
                    'content@': {
                        templateUrl : 'views/slca.html',
                    },
                    'footer': {
                        templateUrl : 'views/footer.html',
                    }
                },
                resolve: {
                    'setUrl': function ($rootScope) {
                        $rootScope.imgUrl = 'images/slca.jpg';
                        $rootScope.home = false;
                        $rootScope.coverText = 'SPEECH AND LANGUAGE';
                    },
                    'setActive' :  function ($rootScope) {
                        $rootScope.hActive = false;
                        $rootScope.mActive = false;
                        $rootScope.aActive = false;
                        $rootScope.slActive = true;
                        $rootScope.haActive = false;
                        $rootScope.ciActive = false;
                        $rootScope.cActive = false;
                    }
                }
            })
            .state('app.slt', {
                url: 'speechlanguagetherapy',
                views: {
                    'header': {
                        templateUrl : 'views/header.html',
                        controller : 'HeaderController'
                    },
                    'content@': {
                        templateUrl : 'views/slt.html',
                    },
                    'footer': {
                        templateUrl : 'views/footer.html',
                    }
                },
                resolve: {
                    'setUrl': function ($rootScope) {
                        $rootScope.imgUrl = 'images/slt.jpg';
                        $rootScope.home = false;
                        $rootScope.coverText = 'SPEECH AND LANGUAGE';
                    },
                    'setActive' :  function ($rootScope) {
                        $rootScope.hActive = false;
                        $rootScope.mActive = false;
                        $rootScope.aActive = false;
                        $rootScope.slActive = true;
                        $rootScope.haActive = false;
                        $rootScope.ciActive = false;
                        $rootScope.cActive = false;
                    }
                }
            })
            /*.state('app.vca', {
                url: 'voicecomprehensiveassessment',
                views: {
                    'header': {
                        templateUrl : 'views/header.html',
                        controller : 'HeaderController'
                    },
                    'content@': {
                        templateUrl : 'views/vca.html',
                    },
                    'footer': {
                        templateUrl : 'views/footer.html',
                    }
                },
                resolve: {
                    'setUrl': function ($rootScope) {
                        $rootScope.imgUrl = 'images/home.jpg';
                    },
                    'setActive' :  function ($rootScope) {
                        $rootScope.hActive = false;
                        $rootScope.mActive = false;
                        $rootScope.aActive = false;
                        $rootScope.slActive = true;
                        $rootScope.haActive = false;
                        $rootScope.ciActive = false;
                        $rootScope.cActive = false;
                    }
                }
            })*/
            .state('app.vt', {
                url: 'voicetherapy',
                views: {
                    'header': {
                        templateUrl : 'views/header.html',
                        controller : 'HeaderController'
                    },
                    'content@': {
                        templateUrl : 'views/vt.html',
                    },
                    'footer': {
                        templateUrl : 'views/footer.html',
                    }
                },
                resolve: {
                    'setUrl': function ($rootScope) {
                        $rootScope.imgUrl = 'images/vt.jpg';
                        $rootScope.home = false;
                        $rootScope.coverText = 'SPEECH AND LANGUAGE';
                    },
                    'setActive' :  function ($rootScope) {
                        $rootScope.hActive = false;
                        $rootScope.mActive = false;
                        $rootScope.aActive = false;
                        $rootScope.slActive = true;
                        $rootScope.haActive = false;
                        $rootScope.ciActive = false;
                        $rootScope.cActive = false;
                    }
                }
            });
    
        $urlRouterProvider.otherwise('/');
    });
}());