(function () {

'use strict';

angular.module('ssApp')
.controller('HeaderController', ['$scope', '$location', '$rootScope', function($scope, $location, $rootScope) {
  $scope.activateHome = function () {
    $rootScope.hActive = true;
    $rootScope.mActive = false;
    $rootScope.aActive = false;
    $rootScope.slActive = false;
    $rootScope.haActive = false;
    $rootScope.ciActive = false;
    $rootScope.cActive = false;
    $rootScope.home = true;
    $rootScope.coverText = 'Explore the universe of sounds and express your heart out to your loved ones';
    $rootScope.imgUrl = 'images/home.jpg';
  };
  $scope.navToggle = function () {
    $scope.aOpen = false;
    $scope.slOpen = false;
    $scope.aAria = false;
    $scope.slAria = false;
    $scope.navAria = !$scope.navAria;
    if (!$scope.navAria) $scope.navState = $scope.navState + ' collapsed';    
    else {
      var arr = $scope.navState.split(" ");
      arr.pop();
      $scope.navState = arr.join("");
    }
  };
  $scope.aToggle = function () {
    $scope.aAria = !$scope.aAria;
    $scope.aOpen = !$scope.aOpen;
    if ($scope.slOpen) {
      $scope.slOpen = false;
      $scope.slAria = false;
    }
  };
  $scope.slToggle = function () {
    $scope.slAria = !$scope.slAria;
    $scope.slOpen = !$scope.slOpen;
    if ($scope.aOpen) {
      $scope.aOpen = false;
      $scope.aAria = false;
    }
  };
  $scope.closeDropdown= function () {
    $scope.aOpen = false;
    $scope.slOpen = false;
    $scope.aAria = false;
    $scope.slAria = false;
  }; 
}])


.controller('IndexController', ['$scope', function($scope) {
  $scope.submitted = false;
  $scope.modalActive = false;
  $scope.errorMsg = false;
  $scope.toggleModal = function () {
    $scope.modalActive = !$scope.modalActive;
  }
  $scope.closeModal = function () {
    $scope.submitted = false;
    $scope.modalActive = false;
  }
  $scope.appointment = function (data, form) {
    var telArr = data.contact.split("");
    var i;
    var len = telArr.length;
    for (i=0; i<len; i++) {
      if (i==0 && telArr[i]=="+") continue;
      if (isNaN(parseInt(telArr[i]))) {
        data.contact = "";
        break;
      }
    }
    if (i==len) {
      if (telArr[0]=="0") data.contact = '+91' + telArr.slice(1,telArr.length).join("");
      else if (telArr[0]=="+") data.contact = telArr.join("");
      else data.contact = '+91' + telArr.join("");
      var sub = 'Appointment Request - ' + data.name;
      var msg = 'Schedule an appointment for ' + data.name + ' with email id - ' + data.email + ' and contact number - ' + data.contact + '.';
      //Mailer Code
      Email.send(
        "info@exaltshc.com",
        "info@exaltshc.com",
        sub,
        msg,
        "smtp.elasticemail.com",
        "info@exaltshc.com",
        "489d8ae9-c865-46ec-823c-45ac635a7b2d",
        function done(message) { 
          console.log("Appointment Request Sent");
          $scope.$apply(function () {
            $scope.submitted = true;
          });
        }
      )
    }
  } 
}])

}());